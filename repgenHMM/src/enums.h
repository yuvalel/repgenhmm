/*
 * enums.h
 *
 *  Created on: 18 Dec 2015
 *      Author: elhanati
 */

#ifndef SRC_ENUMS_H_
#define SRC_ENUMS_H_

///  Enum for numeric codes for state types
enum StateType:int{sG=0,sV=5,sJ=500,sI=700,sN=750,sD=800};
///  Enum for chain type
enum ChainT:int{Heavy,Light,Unknown};



#endif /* SRC_ENUMS_H_ */
