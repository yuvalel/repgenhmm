/*
 * Utils.h
 *
 *  Created on: Apr 9, 2015
 *      Author: quentin
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <fstream>
#include <string>
#include <utility>
#include <tuple>



enum Event_type {GeneChoice_t , Deletion_t , Insertion_t , Dinuclmarkov_t};
enum Event_safety{VD_safe , VD_unsafe , DJ_safe , DJ_unsafe , VJ_safe , VJ_unsafe };
enum Seq_side{ Five_prime , Three_prime , Undefined_side };
enum Seq_type {V_gene_seq , VD_ins_seq , D_gene_seq , DJ_ins_seq , J_gene_seq , VJ_ins_seq};
enum Gene_class{V_gene , VD_genes , D_gene , DJ_genes , J_gene , VJ_genes , VDJ_genes ,Undefined_gene };

std::ostream& operator<<(std::ostream& , Gene_class);
std::ostream& operator<<(std::ostream& , Seq_side);



//Type used as key for unordered map since Rec_event cannot be instantiated
typedef std::string Rec_Event_name;

//Type used for offset of alignmed sequences in sequence_offsets maps. Used to characterize the beginning and the end of a sequence on the data sequence
typedef int Seq_Offset;





/*
 * Defining a hash functions for Rec_Event, Gene_class and pair<Gene_class,Seq_side>
 */
 namespace std{
	 /*
 	 template<>
	 struct hash<Rec_Event>{
		inline std::size_t operator()(const Rec_Event& event) const{ //TODO inline?
			return  (((hash<int>()(event.get_class())
					^(hash<int>()(event.get_side())<<1 )) >>1)
					^(hash<int>()(event.get_priority())<<1)>>1)
					^(hash<int>()(event.get_realizations_map().size())<<1);
			//Note : only consider the size of the realization map and not what it contains for speed purposes
			//this should be enough to ensure no collisions
		}
	 };
	 */


/*
	 template<>
	 struct hash<Rec_Event*>{
		 std::size_t operator()(const Rec_Event*& event_point) const{
			 return hash<Rec_Event>()(*event_point);
		 }
	 };
	 */

	 template<>
	 	 struct hash<Seq_type>{
	 		 std::size_t operator()(const Seq_type& seq_t) const{
	 		 			return  hash<int>()(seq_t);
	 		 		}
	 	 };

	template<>
		 struct hash<Gene_class>{
			 std::size_t operator()(const Gene_class& gene) const{
						return  hash<int>()(gene);
					}
		 };

	 template<>
	 struct hash<std::pair<Gene_class,Seq_side>>{
		 std::size_t operator()(const pair<Gene_class,Seq_side>& gene_pair) const{
		 		 		return  (hash<Gene_class>()(gene_pair.first)
		 		 				^(hash<int>()(gene_pair.second) <<1))>>1;
		 		 	}
	 };

	 template<>
	 struct hash<std::tuple<Event_type,Gene_class,Seq_side>>{
		std::size_t operator()(const std::tuple<Event_type,Gene_class,Seq_side>& event_triplet) const{
			Event_type ev_type;
			Gene_class g_class;
			Seq_side s_side;
			std::tie(ev_type,g_class,s_side) = event_triplet;
			return ((hash<int>()(ev_type)
					^(hash<int>()(g_class) <<1)>>1)
					^(hash<int>()(s_side) <<1));

		}
	 };

	 template<>
	 struct hash<std::pair<Seq_type,Seq_side>>{
		std::size_t operator()(const std::pair<Seq_type,Seq_side> seq_pair) const{
			return  (hash<int>()(seq_pair.first)
					 		 				^(hash<int>()(seq_pair.second) <<1))>>1;
		}
	 };

	 template<>
	 struct hash<Event_safety>{
		 std::size_t operator()(const Event_safety ev_saf) const{
			 return (hash<int>()(ev_saf));
		 }
	 };
 }




#endif /* UTILS_H_ */
