/*
 * StandardModel.cpp
 *
 *  Created on: Feb 27, 2015
 *      Author: yuvalel
 */

#include "StandardModel.h"
#include <algorithm>
#include <iostream>     // std::cout
#include <iterator>     // std::ostream_iterator
#include <random>
using namespace std;

Standard_Model::Standard_Model() : error_rate(0){
	init(0, 0, 0, NULL, NULL, NULL, false);
}

/*Standard_Model::Standard_Model(int mVdel, int mJdel, int mIns,
		const unordered_map<string,string>* Vs,
		const unordered_map<string,string>* Js, bool uniform){
	// Constructor for light chains model - starts heavy chain model constructor with dummy D gene list
	unordered_map<string,string>* Ds;
	*Ds["NoD"]="";
	init(mVdel, mJdel, mIns, Vs, Ds, Js, uniform);
}*/


Standard_Model::Standard_Model(int mVdel, int mJdel, int mIns,
		 unordered_map<string,string>* Vs,
		 unordered_map<string,string>* Ds,
		 unordered_map<string,string>* Js, bool uniform){
	init(mVdel, mJdel, mIns, Vs, Ds, Js, uniform);
}

/*Standard_Model::Standard_Model(Standard_Model SM){
	init(SM.maxVdel, SM.maxJdel, SM.maxIns, SM.Vs, SM.Ds, SM.Js, false);
}*/

void Standard_Model::init(int mVdel, int mJdel, int mIns,
		unordered_map<string,string>* Vs_input,
		unordered_map<string,string>* Ds_input,
		unordered_map<string,string>* Js_input, bool uniform){

	Vs = Vs_input;
	Ds = Ds_input;
	Js = Js_input;

	chain_type = (Ds->find("NoD")==Ds->end()?Heavy:Light); //sets chain type
	double F=uniform; //if uniform, F=1. otherwise F=0 and blank model is produced

	error_rate = 0.01*F;
	error_count = 0;
	genomic_count = 0;

	maxVdel = mVdel;
	maxJdel = mJdel;
	maxIns = mIns;
	minDlen = 2;

	nt_bias['A'] = 0.25*F;
	nt_bias['G'] = 0.25*F;
	nt_bias['C'] = 0.25*F;
	nt_bias['T'] = 0.25*F;

	nt_biasVD['A'] = 0.25*F;
	nt_biasVD['G'] = 0.25*F;
	nt_biasVD['C'] = 0.25*F;
	nt_biasVD['T'] = 0.25*F;

	nt_biasDJ['A'] = 0.25*F;
	nt_biasDJ['G'] = 0.25*F;
	nt_biasDJ['C'] = 0.25*F;
	nt_biasDJ['T'] = 0.25*F;

	pins = vector<double> (maxIns+1, F/(maxIns+1));

	pinsVD = vector<double> (maxIns+1, F/(maxIns+1));

	pinsDJ = vector<double> (maxIns+1, F/(maxIns+1));

	for (auto& vit : *Vs) pdelV[vit.first] = vector<double> (maxVdel+1, F/(maxVdel+1));
	for (auto& dit : *Ds){
		int d=dit.second.length();
		pdelD[dit.first] = std::vector<std::vector<double>> (d+1,std::vector<double>(d+1)); //vector of vectors for bi-directional D deletions
		int maxDdel = dit.second.length() + 1;
		int numDdelcomb = maxDdel*(maxDdel+1)/2;
		for (int l=0;l<maxDdel;l++) pdelD[dit.first][l] = vector<double> (maxDdel-l, F/numDdelcomb);
	}
	for (auto& jit : *Js) pdelJ[jit.first] = vector<double> (maxJdel+1, F/(maxJdel+1));

	int N_VDJ = Vs->size() * Ds->size() * Js->size();

	for (auto& vit : *Vs)
		for (auto& dit : *Ds)
			for (auto& jit : *Js)
				pVDJ[make_tuple(vit.first, dit.first, jit.first)] = F/N_VDJ;

}

Standard_Model::Standard_Model(Model_allVJ mod){

	init(mod.maxVdel, mod.maxJdel, mod.maxIns, mod.Vs, mod.Ds, mod.Js, false);

	error_count = mod.error_rate;
	genomic_count = mod.error_rate + mod.non_error_rate;

	mod.model_from_counter();

	minDlen = mod.minDlen;

	error_rate = mod.error_rate;
	nt_biasVD = mod.nt_biasVD;
	nt_biasDJ = mod.nt_biasDJ;
	nt_bias = mod.nt_bias;
	pVDJ = mod.pVDJ;
	minDlen = mod.minDlen;
	for (auto& V : mod.transV){
		double tmp = 1;
		pdelV[V.first] = vector<double>(maxVdel+1);
		for (int v=0; v<mod.lenV.at(V.first) ; v++){
			if (V.second.T(sG,1,sV,v)>0){
				pdelV[V.first][mod.lenV.at(V.first)-v-1] = V.second.T(sG,1,sV,v) * tmp;
				tmp *=  V.second.T(sV,v+1,sV,v);
			}
		}
		//reverse(pdelV[V.first].begin(), pdelV[V.first].end());
	}


	if (chain_type == Heavy){
		double tmp;

		tmp=0;
		pinsVD = vector<double>(maxIns);
		pinsVD[0] = mod.trans.T(sG,2,sG,1);
		tmp = mod.trans.T(sI,0,sG,1);
		for (int i=1; tmp>0 ; i++){
			pinsVD[i] = mod.trans.T(sG,2,sI,i-1) * tmp; //i insertions corresponds to the transition from I(i-1) to G2
			tmp *=  mod.trans.T(sI,i,sI,i-1);
		}

		for (auto& D : mod.transD){
			int d=mod.lenD.at(D.first);
			pdelD[D.first] = std::vector<std::vector<double>> (d+1,std::vector<double>(d+1));
			for (int l=0;l<=d;l++) //TODO: maybe make vectors shorter to create trig matrix
				for (int r=0;r<=d-l;r++)
					pdelD[D.first][l][r] = D.second.T(sD,0,l,r,sG,2);
		}
		tmp=0;
		pinsDJ = vector<double>(maxIns);
		pinsDJ[0] = mod.trans.T(sG,4,sG,3);
		tmp = mod.trans.T(sN,0,sG,3);
		for (int i=1; tmp>0 ; i++){
			pinsDJ[i] = mod.trans.T(sG,4,sN,i-1) * tmp; //i insertions corresponds to the transition from I(i-1) to G2
			tmp *=  mod.trans.T(sN,i,sN,i-1);
		}
	}

	if (chain_type == Light){
		pins = vector<double>(maxIns);
		pins[0] = mod.trans.T(sG,4,sG,1);
		double tmp = mod.trans.T(sI,0,sG,1);
		for (int i=1; tmp>0 ; i++){
			pins[i] = mod.trans.T(sG,4,sI,i-1) * tmp; //i insertions corresponds to the transition from I(i-1) to G2
			tmp *=  mod.trans.T(sI,i,sI,i-1);
		}
	}

	for (auto& J : mod.transJ){
		pdelJ[J.first] = vector<double>(maxJdel+1);
		for (int j=0; J.second.T(sJ,j,sG,4)>0 ; j++)
			pdelJ[J.first][j] = J.second.T(sJ,j,sG,4);
	}
}


/*vector<pair<const int, const string>> Standard_Model::generate_sequences(int N,
		const unordered_map<string,string>& Vs,
		const unordered_map<string,string>& Js,
		unsigned seed1, bool with_err, ostream& strm) const{
	unordered_map<string,string> Ds;
	Ds["NoD"]="";
	return generate_sequences(N, Vs, Ds, Js, seed1, with_err, strm);
}*/

vector<pair<const int, const string>> Standard_Model::generate_sequences(int N,
		unsigned seed1, bool with_err, ostream& strm) const{

    default_random_engine generator =  default_random_engine(seed1);
    discrete_distribution<int> I_dist (pins.begin() , pins.end());

    discrete_distribution<int> IVD_dist (pinsVD.begin() , pinsVD.end());
    discrete_distribution<int> IDJ_dist (pinsDJ.begin() , pinsDJ.end());

    unordered_map<string, discrete_distribution<int>> Vdel_dist;
    for (auto& it: pdelV)
    	Vdel_dist[it.first] = discrete_distribution<int> (it.second.begin() , it.second.end());

    unordered_map<string, discrete_distribution<int>> Jdel_dist;
    for (auto& it: pdelJ)
    	Jdel_dist[it.first] = discrete_distribution<int> (it.second.begin() , it.second.end());

    unordered_map<string, discrete_distribution<int>> Ddel_dist;
    if (chain_type==Heavy){
    	for (auto& it: pdelD){
    		vector<double> pdelD_p;
    		for (unsigned l=0;l<Ds->at(it.first).length();l++) //it.second.size()
    			for (unsigned r=0;r<Ds->at(it.first).length();r++) //it.second.at(l).size()
    				pdelD_p.push_back(it.second.at(l).at(r));
    		Ddel_dist[it.first] = discrete_distribution<int> (pdelD_p.begin() , pdelD_p.end());
    	}
    }

    vector<double> nt_bias_p;
    string nt_bias_order;

    if (chain_type==Light)
    for (auto& it: nt_bias){
    	nt_bias_p.push_back( it.second );
    	nt_bias_order.push_back( it.first );
    }

    vector<double> nt_biasVD_p;
    string nt_biasVD_order;
    vector<double> nt_biasDJ_p;
    string nt_biasDJ_order;

    if (chain_type==Heavy){
    	for (auto& it: nt_biasVD){
    		nt_biasVD_p.push_back( it.second );
    		nt_biasVD_order.push_back( it.first );
    	}
    	for (auto& it: nt_biasDJ){
    		nt_biasDJ_p.push_back( it.second );
    		nt_biasDJ_order.push_back( it.first );
    	}
    }

    discrete_distribution<int> nt_dist (nt_bias_p.begin() , nt_bias_p.end());
    discrete_distribution<int> nt_distVD (nt_biasVD_p.begin() , nt_biasVD_p.end());
    discrete_distribution<int> nt_distDJ (nt_biasDJ_p.begin() , nt_biasDJ_p.end());

    vector<double> pVDJ_p;
    vector<tuple<string,string,string>> pVDJ_p_order;
    for (auto& it: pVDJ){
    	pVDJ_p.push_back( it.second );
    	pVDJ_p_order.push_back( it.first );
    }

    discrete_distribution<int> VDJ_dist (pVDJ_p.begin() , pVDJ_p.end());

    bernoulli_distribution error_dist (error_rate);
    uniform_int_distribution<int> nt_offset_dist(1,3); //for an error, any base but the correct one is equaly likeli

    vector<pair<const int, const string>> seq; //vector for generated sequences

    if (chain_type==Light) strm << "seq#;V;J;Vdel;Jdel;Nins;insert;" << endl;
    if (chain_type==Heavy) strm << "seq#;V;D;J;Vdel;Jdel;NinsVD;VDinsert;NinsDJ;DJinsert" << endl;

	for (int k=0;k<N;k++){ //loop over generated sequences
		if ((k % 10000) == 0) cout << "seq #" << k << endl;
		
		int VDJn = VDJ_dist(generator); //# of V,D,J choice
		auto VDJ = pVDJ_p_order.at(VDJn); //triplets of V,D,J choice
		string V = get<0>(VDJ);
		string D = get<1>(VDJ);
		string J = get<2>(VDJ);

		int Vdel = Vdel_dist[V](generator);
		int Jdel = Jdel_dist[J](generator);

		string Vseq = Vs->at(V).substr(0, Vs->at(V).length()-Vdel); //V seq deleted from right
		string Jseq = Js->at(J).substr(Jdel); //J seq deleted from left

		if (with_err){ //adding errors to V,J segments
			for (auto& it: Vseq)
				if (error_dist(generator))
					it = nts.at((nts.find(it) + nt_offset_dist(generator)) % 4);
			for (auto& it: Jseq)
				if (error_dist(generator))
					it = nts.at((nts.find(it) + nt_offset_dist(generator)) % 4);
		}

		if (chain_type==Light){
			int I = I_dist(generator);
			string insert(I,'N'); //string to hold I insertions
			for (int i=0;i<I;i++){
				int nt = nt_dist(generator);
				insert[i] = nt_bias_order.at(nt);
			}
			seq.push_back (make_pair(k, Vseq + insert + Jseq));
			strm << k << ";" << V << ";" << J << ";" << Vdel << ";" << Jdel << ";" << I << ";" << insert << ";" << endl;
		}

		if (chain_type==Heavy){
			int Ddel = Ddel_dist[D](generator); //generate number of right and left D dels
			int DdelL = (int)Ddel / Ds->at(D).length();
			int DdelR = Ddel % Ds->at(D).length();

			string Dseq = Ds->at(D).substr(DdelL, Ds->at(D).length()-DdelR);//D seq deleted from right and left

			if (with_err)  //adding errors to D segment
				for (auto& it: Dseq)
					if (error_dist(generator))
						it = nts.at((nts.find(it) + nt_offset_dist(generator)) % 4);

			int IVD = IVD_dist(generator);
			int IDJ = IDJ_dist(generator);
			string insertVD(IVD,'N'); //string to hold VD insertions
			for (int i=0;i<IVD;i++){
				int nt = nt_distVD(generator);
				insertVD[i] = nt_biasVD_order.at(nt);
			}
			string insertDJ(IDJ,'N'); //string to hold DJ insertions
			for (int i=0;i<IDJ;i++){
				int nt = nt_distDJ(generator);
				insertDJ[i] = nt_biasDJ_order.at(nt);
			}

			seq.push_back (make_pair(k, Vseq + insertVD + Dseq + insertDJ + Jseq));
			strm << k << ";" << V << ";" << D << ";" << J << ";" << Vdel << ";" << Jdel
					<< ";" << IVD << ";" << insertVD << ";" << IDJ << ";" << insertDJ << ";" << endl;
		}
	}
	return seq;
}

void Standard_Model::output(ostream& strm, string output_features){

	bool sec = (output_features.empty()?true:output_features.find(",")!=string::npos); //true if outputting more than one feature and need section headings


	if (output_features.empty() || output_features.find("PVJ")!=string::npos)
		if (chain_type==Light){
			if (sec) strm << "#PVJ:" << endl;
			for (const auto& it : pVDJ)
				strm << get<0>(it.first) << " " << get<2>(it.first) << " " << it.second << endl;
		}
	if (output_features.empty() || output_features.find("PVDJ")!=string::npos)
		if (chain_type==Heavy){
			if (sec) strm << "#PVDJ:" << endl;
			for (const auto& it : pVDJ)
				strm << get<0>(it.first) << " " << get<1>(it.first) << " " << get<2>(it.first) << " " << it.second << endl;
		}


	ostream_iterator<double> Writer(strm," ");
	if (output_features.empty() || output_features.find("PVdel")!=string::npos){
		if (sec) strm << "#PVdel:" << endl;
		for (const auto& it : pdelV){
			strm << it.first << endl;
			copy(it.second.cbegin(), it.second.cend(), Writer);
			strm << endl;
		}
	}
	if (output_features.empty() || output_features.find("PDdel")!=string::npos)
		if (chain_type==Heavy){
			if (sec) strm << "#PDdel:" << endl;
			for (const auto& it : pdelD){
				strm << it.first << endl;
				for (unsigned l=0;l<it.second.size();l++) {
					copy(it.second.at(l).cbegin(), it.second.at(l).cend(), Writer);
					strm << " |" << endl;
				}
			}
		}


	if (output_features.empty() || output_features.find("PJdel")!=string::npos){
		if (sec) strm << "#PJdel:" << endl;
		for (const auto& it : pdelJ){
			strm << it.first  << endl;
			copy(it.second.cbegin(), it.second.cend(), Writer);
			strm << endl;
		}
	}


	if (chain_type==Light){
		if (output_features.empty() || output_features.find("Ins")!=string::npos){
			if (sec) strm << "#Ins:" << endl;
			copy(pins.cbegin(), pins.cend(), Writer);
			strm << endl;
		}

		if (output_features.empty() || output_features.find("nt_bias")!=string::npos){
			if (sec) strm << "#nt_bias:" << endl;
			for (const auto& it : nt_bias){
				strm << it.first << " " << it.second << endl;
			}
		}
	}

	if (chain_type==Heavy){
		if (output_features.empty() || output_features.find("VDIns")!=string::npos){
			if (sec) strm << "#VDIns:" << endl;
			copy(pinsVD.cbegin(), pinsVD.cend(), Writer);
			strm << endl;
		}
		if (output_features.empty() || output_features.find("DJIns")!=string::npos){
			if (sec) strm << "#DJIns:" << endl;
			copy(pinsDJ.cbegin(), pinsDJ.cend(), Writer);
			strm << endl;
		}

		if (output_features.empty() || output_features.find("nt_biasVD")!=string::npos){
			if (sec) strm << "#nt_biasVD:" << endl;
			for (const auto& it : nt_biasVD){
				strm << it.first << " " << it.second << endl;
			}
		}
		if (output_features.empty() || output_features.find("nt_biasDJ")!=string::npos){
			if (sec) strm << "#nt_biasDJ:" << endl;
			for (const auto& it : nt_biasDJ){
				strm << it.first << " " << it.second << endl;
			}
		}
	}

	if (output_features.empty() || output_features.find("error_rate")!=string::npos){
		if (sec) strm << "#error_rate:" << endl;
		strm << error_rate << endl;
	}

	if (output_features.find("error_count")!=string::npos){
		if (sec) strm << "#error_count:" << endl;
		strm << error_count << endl;
	}
}

void Standard_Model::read(istream& strm){
	if(!strm){
		cout << "Model File not found" << endl;
		exit(1);
	}

	string tmp;
	strm >> tmp;
	if (tmp=="#PVJ:") chain_type=Light;
	else if (tmp=="#PVDJ:") chain_type=Heavy;
	else {
		cout << "Model File corrupted on line: " << tmp << endl;
		exit(1);
	}
	//reading V(D)J choices
	strm >> tmp;
	while (tmp[0]!='#'){
		string V = tmp;
		strm >> tmp;
		string D = "NoD"; //default value for light chains
		if (chain_type==Heavy){
			D = tmp;
			strm >> tmp;
		}
		string J = tmp;
		strm >> tmp;
		pVDJ[make_tuple(V,D,J)] = stod(tmp);
		strm >> tmp;
	}
	//reading V deletions;

	if (tmp!="#PVdel:"){
		cout << "Model File corrupted on line: " << tmp << endl;
		exit(1);
	}
	strm >> tmp;
	while (tmp[0]!='#'){
		string V = tmp;
		strm >> tmp;
		pdelV[V] = vector<double>();
		while (isdigit(tmp[0])){
			pdelV[V].push_back(stod(tmp));
			strm >> tmp;
		}
	}

	//reading D deletions
	if (chain_type==Heavy){
		if (tmp!="#PDdel:"){
			cout << "Model File corrupted on line: " << tmp << endl;
			exit(1);
		}
		strm >> tmp;
		while (tmp[0]!='#'){
			string D = tmp;
			int l = 0;
			strm >> tmp;
			while (isdigit(tmp[0])){
				while (tmp[0]!='|'){
					//cout << "writing: " << tmp << endl;
					pdelD[D].at(l).push_back(stod(tmp));
					strm >> tmp;
				}
				l++;
				strm >> tmp;
			}
		}
	}

	//reading J deletions
	if (tmp!="#PJdel:"){
		cout << "Model File corrupted on line: " << tmp << endl;
		exit(1);
	}
	strm >> tmp;
	while (tmp[0]!='#'){
		string J = tmp;
		strm >> tmp;
		pdelJ[J] = vector<double>();
		while (isdigit(tmp[0])){
			pdelJ[J].push_back(stod(tmp));
			strm >> tmp;
		}
	}

	if (chain_type==Light){
		//reading insertions
		if (tmp!="#Ins:"){
			cout << "Model File corrupted on line: " << tmp << endl;
			exit(1);
		}
		strm >> tmp;
		pins = vector<double>();
		while (tmp[0]!='#'){
			pins.push_back(stod(tmp));
			strm >> tmp;
		}

		//reading nt bias
		if (tmp!="#nt_bias:"){
			cout << "Model File corrupted on line: " << tmp << endl;
			exit(1);
		}
		strm >> tmp;
		while (tmp[0]!='#'){
			char nt = tmp[0];
			strm >> tmp;
			nt_bias[nt] = stod(tmp);
			strm >> tmp;
		}

	}

	if (chain_type==Heavy){
		//reading insertions
		if (tmp!="#VDIns:"){
			cout << "Model File corrupted on line: " << tmp << endl;
			exit(1);
		}
		strm >> tmp;
		pinsVD = vector<double>();
		while (tmp[0]!='#'){
			pinsVD.push_back(stod(tmp));
			strm >> tmp;
		}

		if (tmp!="#DJIns:"){
			cout << "Model File corrupted on line: " << tmp << endl;
			exit(1);
		}
		strm >> tmp;
		pinsDJ = vector<double>();
		while (tmp[0]!='#'){
			pinsDJ.push_back(stod(tmp));
			strm >> tmp;
		}
		//reading nt bias

		if (tmp!="#nt_biasVD:"){
			cout << "Model File corrupted on line: " << tmp << endl;
			exit(1);
		}

		strm >> tmp;
		while (tmp[0]!='#'){
			char nt = tmp[0];
			strm >> tmp;
			nt_biasVD[nt] = stod(tmp);
			strm >> tmp;
		}
		if (tmp!="#nt_biasDJ:"){
			cout << "Model File corrupted on line: " << tmp << endl;
			exit(1);
		}
		strm >> tmp;
		while (tmp[0]!='#'){
			char nt = tmp[0];
			strm >> tmp;
			nt_biasDJ[nt] = stod(tmp);
			strm >> tmp;
		}
	}
	//reading error rate
	if (tmp!="#error_rate:"){
		cout << "Model File corrupted on line: " << tmp << endl;
		exit(1);
	}
	strm >> tmp;
	error_rate = stod(tmp);
}
