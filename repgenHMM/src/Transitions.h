/*
 * Transitions.h
 *
 *  Created on: Feb 17, 2015
 *      Author: yuvalel
 */

#ifndef TRANSITIONS_H_
#define TRANSITIONS_H_

#include <unordered_map>
#include <vector>
#include <tuple>
#include <iostream>
#include "Utils.h"
#include "enums.h"

/// Class that stores probabilities or counts for all possible transitions between states
class Transitions {
protected:
	//std::vector<double> vals;

public:

	/// vector for transition probabilities between states of the same type
	std::vector<double> within;
	/// vector for transition probabilities between states of different types
	std::vector<double> between;

	/// maximum state number
	static int max_state;
	/// maximum number of D deletions
	int max_dlen=20;

	Transitions();
	/// returns ref to transition from state with Type t1 and Number n1, and state with Type t2 and Number n2
	double& T(StateType t1, int n1, StateType t2, int n2);
	/// returns value of transition from state with Type t1 and Number n1, and state with Type t2 and Number n2
	double T(StateType t1, int n1, StateType t2, int n2) const;
	/// divides by factor the value of the transition from state with Type t1 and Number n1, and state with Type t2 and Number n2
	void divide_by(StateType t1, int n1, StateType t2, int n2, double factor);

	/// returns ref to transition from D-state with Type t1, Number n1 and left/right D-deletions l1/r1, and state with Type t2 and Number n2
	double& T(StateType t1, int n1, int l1, int r1, StateType t2, int n2);
	/// returns value of transition from D-state with Type t1, Number n1 and left/right D-deletions l1/r1, and state with Type t2 and Number n2
	double T(StateType t1, int n1, int l1, int r1, StateType t2, int n2) const;
	/// divides by factor the value of the transition from D-state with Type t1, Number n1 and left/right D-deletions l1/r1, and state with Type t2 and Number n2
	void divide_by(StateType t1, int n1, int l1, int r1, StateType t2, int n2, double factor);


	/// adds to current transitions all transitions in another Transition object s, multiplied by factor
	void add(const Transitions& s, double factor=1);
	/// multiplied by factor all current transitions
	void mult(double factor);
	/// set current empty transitions to value of transitions in another Transition object s
	void merge(const Transitions& s);
	/// set all current transitions to value of transitions in another Transition object s
	void copy_vals(const Transitions& s);
	/// normalize Transition object so all transition leading from a state sum to 1
	void normalize(ChainT ct=Light);
	/// separate Transition object into 4 objects - for transitions involving V, D or J states, or no genomic states at all
	void seperate(Transitions& T, Transitions& TV, Transitions& TD, Transitions& TJ) const;
	/// compare two transition objects according to threshold
	bool compare(const Transitions& T, double thrs);
	/// Writes transitions to stream
	void write(std::ostream& strm);
	/// Reads transitions from stream
	void read(std::istream& strm);
};


#endif /* TRANSITIONS_H_ */
