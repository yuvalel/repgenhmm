/*
 * main.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: elhanati
 */

#include "infer_funcs.h"
#include "Transitions.h"
#include "Model_1VJ.h"
#include "ModelallVJ.h"
#include "infer_HMM.h"
#include <iostream>
#include <vector>
#include <tuple>
#include <unordered_set>
#include <chrono>
using namespace std;

/// Maximum State number for all transition objects. Change by chain type.
int Transitions::max_state;

void testing(string folder, ChainT chain_type, unordered_map<string,string>& Vs,  unordered_map<string,string>& Ds,
		 unordered_map<string,string>& Js, bool gen_ref);

int main (int argc , char* argv[]){

	bool test=false, gen_ref=false;

	string work_folder = "./",results_path="", data_folder ="" ;
	string pool="", pool_name="", suf="", model_file="", geno = "data/genomic/TRA", features="";
	string V_alig_file = "", J_alig_file = "", seq_indx_file ="", seq_file="";
	ChainT chain_type = Unknown;
	int N = 0, ins = 40, Vdel = 40, Jdel = 30, max_iter = 10, thrds = 4, plen=0, read_len = 100;
	double Vthres = 0, Jthres = 0, Vrange = -1, Jrange = -1;
	bool with_err = true;
	bool alig = false, infer = false, generate = false, stat_alig =  false, prob =  false, history = false, dbg = false;
	unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
	//cout  << "parsing command line switches" << endl;
	cout << argv[0] << endl;
	for (int i = 1; i < argc; i++) {
			if (string(argv[i]) == "-folder") { //work folder path
				work_folder = argv[i + 1];
			} else if (string(argv[i]) == "-pool") { // reads pool name - sets all other file names
				pool = argv[i + 1];
			} else if (string(argv[i]) == "-seq") { // raw sequence file
				seq_file = argv[i + 1];
			} else if (string(argv[i]) == "-idxseq") { // index sequence file
				seq_indx_file = argv[i + 1];
			} else if (string(argv[i]) == "-Valig") { // V alignment file
				V_alig_file = argv[i + 1];
			} else if (string(argv[i]) == "-Jalig") { // J alignment file
				J_alig_file = argv[i + 1];
			} else if (string(argv[i]) == "-geno") { //genomic sequences path and start of file name
				geno = argv[i + 1];
			} else if (string(argv[i]) == "-model") { //model file name. uniform model used if not given
				model_file = argv[i + 1];
			} else if (string(argv[i]) == "-results") { //model file name. uniform model used if not given
				results_path = argv[i + 1];
			} else if (string(argv[i]) == "-suf") { //suffix to add to model file
				suf = argv[i + 1];
			} else if (string(argv[i]) == "-n") { //number of reads to use or generate
				N = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-readlen") { //read length to generate
				read_len = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-iter") { //number of iterations for inference
				max_iter = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-th") { //number of threads to use
				thrds = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-ins") { // max insertion
				ins = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-Vdel") { // max V deletions
				Vdel = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-Jdel") { // max J deletions
				Jdel = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-pali") {
				plen = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-Vthres") { //threshold on V alignment score
				Vthres = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-Jthres") { // threshold on J alignment score
				Jthres = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-Vrange") {
				Vrange = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-Jrange") {
				Jrange = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-light" || string(argv[i]) == "-alpha") { //switch for light/alpha
				chain_type = Light;
			} else if (string(argv[i]) == "-heavy" || string(argv[i]) == "-beta") { //switch for heavy/beta
				chain_type = Heavy;
			} else if (string(argv[i]) == "-a") { //switch for aligning
				alig = true;
			} else if (string(argv[i]) == "-i") { //switch for inference
				infer = true;
			} else if (string(argv[i]) == "-g") { //switch for generating
				generate = true;
			} else if (string(argv[i]) == "-seed") { // threshold on J alignment score
				seed1 = stoi(argv[i + 1]);
			} else if (string(argv[i]) == "-noerr") { //switch for generating sequences without errors
				with_err = false;
			} else if (string(argv[i]) == "-stat") { //switch for outputting alignment stats
				stat_alig = true;
			} else if (string(argv[i]) == "-prob") { //switch for calculating probabilities of sequences according to model
				prob = true;
			} else if (string(argv[i]) == "-history") { //switch for calculating probabilities of sequences according to model
				history = true;
			} else if (string(argv[i]) == "-features") { // reads features to export for every sequence
				features = argv[i + 1];
			} else if (string(argv[i]) == "-debug") { //switch for debug mode
				dbg = true;
			} else if (string(argv[i]) == "-test") { //switch for self test
				test = true;
			} else if (string(argv[i]) == "-test_gen") { //switch for generating self test references
				test = true;
				gen_ref = true;
			} else if (argv[i][0]=='-') {
				cout << endl << "error parsing arguments. switch " << argv[i] << " not recognized."  << endl;
				exit(0);
			}
		std::cout << argv[i] << " ";
	}
	cout << endl;

	omp_set_dynamic(0);
	omp_set_num_threads(thrds); //setting number of threads to use for all parallel sections


	if (chain_type==Unknown){
		std::cout << "chain type has to be specified, please resubmit" << endl;
		exit(0);
	}

	Transitions::max_state = (chain_type==Light?1000:2000); //set Transition object max state number according to chain type

	// reading genomic data

	auto header2name = [] (vector<pair<string,string>>& genomic){
		for (auto& it : genomic){
			it.first.erase(0, it.first.find("|") + 1);
			it.first.erase(it.first.find("|"));
		};
	};

	auto rev_comp = [] (string seq) -> string{
		for (auto & nt:seq){
			switch (nt){
			case 'A': nt = 'T';break;
			case 'T': nt = 'A';break;
			case 'G': nt = 'C';break;
			case 'C': nt = 'G';break;
			}
		}
		reverse(begin(seq), end(seq));
		return seq;
	};

	unordered_map<string,string> Vs;
	unordered_map<string,string> Ds;
	unordered_map<string,string> Js;

	string Vgen = work_folder + geno + "V.fasta";
	cout << "reading V segments" << endl;
	vector<pair<string,string>> v_genomic = read_genomic_fasta(Vgen);
	if (v_genomic[0].first.find("|") != string::npos) header2name(v_genomic);

	//convert genomic vectors of pairs to maps
	for (auto it : v_genomic) {
		Vs.insert(it);
		it.second = it.second + rev_comp(it.second.substr(it.second.length() - plen));
	}

	if (chain_type==Heavy){
		string Dgen = work_folder + geno + "D.fasta";
		cout << "reading D segments" << endl;
		vector<pair<string,string>> d_genomic = read_genomic_fasta(Dgen);
		if (d_genomic[0].first.find("|") != string::npos) header2name(d_genomic);
		for (auto it : d_genomic) {
			it.second = rev_comp(it.second.substr(0, plen)) + it.second + rev_comp(it.second.substr(it.second.length() - plen));
			Ds.insert(it);
		}
	}

	if (chain_type==Light){
		Ds["NoD"]="";
	}

	string Jgen = work_folder + geno + "J.fasta";
	cout << "reading J segments" << endl;
	vector<pair<string,string>> j_genomic = read_genomic_fasta(Jgen);
	if (j_genomic[0].first.find("|") != string::npos) header2name(j_genomic);
	for (auto it : j_genomic) {
		it.second = rev_comp(it.second.substr(0, plen)) + it.second;
		Js.insert(it);
	}

	cout << "number of V genes: " << Vs.size()  << endl;
	if (chain_type==Heavy) cout << "number of D genes: " << Ds.size()  << endl;
	cout << "number of J genes: " << Js.size()  << endl;


	if (test){
		testing(work_folder, chain_type, Vs, Ds, Js, gen_ref);
		exit(0);
	}


	// setting folders and files names
	if (pool.empty()){
		std::cout << "pool has to be specified, please resubmit" << endl;
		exit(0);
	}
	if (generate) pool = pool + "_synth" + to_string(seed1);
	string::size_type pos = pool.rfind('/'); //if pool is a path+name, then pool name is the name without the path
	pool_name = (pos==string::npos?pool:pool.substr(pos+1));
	if (results_path.empty() && pos!=string::npos) results_path = pool.substr(0,pos+1); //if no results folder is given and there is folder for the pool, use this

	double nuc44_vect [] = {5,-4,-4,-4 , -4 ,5,-4,-4 , -4,-4,5,-4 , -4,-4,-4,5};
	Matrix<double> nuc44_sub_matrix(4,4,nuc44_vect);

	//loading model or creating a uniform one
	Standard_Model std_mod(Vdel, Jdel, ins, &Vs, &Ds, &Js, true);
	if (!model_file.empty()){
		ifstream modelfile(work_folder + model_file);
		std_mod.read(modelfile);
	}

	vector<pair<const int, const string>> indexed_seqlist_all;

	//cout << "data folder is " << data_folder << endl;

	if (V_alig_file.empty()) V_alig_file = work_folder + pool + "_V_alig.csv";
	else V_alig_file = work_folder + V_alig_file;
	if (J_alig_file.empty()) J_alig_file = work_folder + pool + "_J_alig.csv";
	else J_alig_file = work_folder + J_alig_file;
	if (seq_indx_file.empty()) seq_indx_file = work_folder + pool + "_indx.csv";
	else seq_indx_file = work_folder + seq_indx_file;

	if (generate){
		if (N==0) N = 100000; // if number of seq were not specified, generate 10,000

		string gen_file_no_ext = seq_indx_file.substr(0, seq_indx_file.find('.'));

		cout << "Generating sequences" << endl;
		cout << "seed: " << std::to_string(seed1) << endl;
		cout << "Saving generation data to: " << gen_file_no_ext + "_gendata.csv" << endl;
		ofstream genfile(gen_file_no_ext + "_gendata.csv");
		vector<pair<const int, const string>> indexed_seqlist_all_full =
				std_mod.generate_sequences(N, seed1, with_err, genfile);
		write_indexed_seq_csv(gen_file_no_ext + "_full.csv", indexed_seqlist_all_full);
		for (auto& it : indexed_seqlist_all_full) //cut seq to read_len length, from start of J
			indexed_seqlist_all.push_back(make_pair(it.first,it.second.substr(it.second.length() - read_len)));
		write_indexed_seq_csv(seq_indx_file, indexed_seqlist_all);

	}
	else { // if not generating, load sequences
		ifstream f(seq_indx_file);
		if (f.good()){
			f.close();
			cout << "reading aligned and indexed sequences: " <<  seq_indx_file << endl;
			indexed_seqlist_all = read_indexed_csv(seq_indx_file);
		}
		else {
			f.close();
			if (seq_file.empty()) {
				//seq_file = data_folder + "non_productive/" + pool + ".txt";
				cout << "missing sequence file" << endl;
				exit(1);
			}
			seq_file = work_folder + seq_file;
			ifstream f(seq_file);
			if (f.good()){
				f.close();
				cout << "loading sequences" << endl;
				string lastfive = seq_file.substr(seq_file.length() - 5);
				transform(lastfive.begin(),lastfive.end(),lastfive.begin(),::tolower);
				if (lastfive=="fasta")
					indexed_seqlist_all = read_fasta(seq_file);
				else
					indexed_seqlist_all = read_txt(seq_file);
				write_indexed_seq_csv(seq_indx_file, indexed_seqlist_all);
			}
			else {
				cout << "sequence file not found at" << endl << seq_indx_file << endl << " or " << seq_file << endl;
				f.close();
				exit(1);
			}
		}

	}

	if (alig){
		Aligner v_aligner(nuc44_sub_matrix, 50, V_gene);
		v_aligner.set_genomic_sequences(v_genomic);

		Aligner j_aligner(nuc44_sub_matrix, 50, J_gene);
		j_aligner.set_genomic_sequences(j_genomic);

		cout << "number of sequences to align: " << indexed_seqlist_all.size()  << endl;

		cout << "Aligning to J genes" << endl;
		unordered_map<int,forward_list<Alignment_data>> j_alignments =
				j_aligner.align_seqs(indexed_seqlist_all,Jthres,true);
		j_aligner.write_alignments_seq_csv(J_alig_file, j_alignments);
		j_alignments.clear();

		cout << "Aligning to V genes" << endl;
		unordered_map<int,forward_list<Alignment_data>> v_alignments =
				v_aligner.align_seqs(indexed_seqlist_all,Vthres,true);
		v_aligner.write_alignments_seq_csv(V_alig_file, v_alignments);
		v_alignments.clear();
	}

	if (stat_alig){
		unordered_map<int,pair<string,unordered_map<Gene_class,vector<Alignment_data>>>> best_alig;
		best_alig = read_alignments_seq_csv_score_range(V_alig_file, V_gene, 0, true, indexed_seqlist_all, best_alig);
		best_alig = read_alignments_seq_csv_score_range(J_alig_file, J_gene, 0, true, indexed_seqlist_all, best_alig);
		ofstream outfile_ba(work_folder + pool + "_best_alignments.txt");
		outfile_ba << "index;sequence;V gene;V score;J gene;J score;" << endl;
		for (auto& it : best_alig)
			if (!it.second.second[V_gene].empty() && !it.second.second[J_gene].empty()){
				outfile_ba << it.first << ";" << it.second.first << ";";
				outfile_ba << it.second.second[V_gene][0].gene_name <<  ";" << it.second.second[V_gene][0].score << ";";
				outfile_ba << it.second.second[J_gene][0].gene_name <<  ";" << it.second.second[J_gene][0].score << ";";
				outfile_ba << endl;
			}
	}

	if (infer || prob){
		cout << "loading alignments" << endl;
		unordered_map<int,vector<Alignment_data>> v_alignments = read_alignments_seq_csv(V_alig_file, Vthres, false);
		unordered_map<int,vector<Alignment_data>> j_alignments = read_alignments_seq_csv(J_alig_file, Jthres, false);


		if (Vrange < 0) //if Vrange negative, use hard threshold to filter alignments
			v_alignments = read_alignments_seq_csv(V_alig_file, Vthres, false);
		else{
			unordered_map<int,pair<string,unordered_map<Gene_class,vector<Alignment_data>>>> sorted_alignments;
			sorted_alignments = read_alignments_seq_csv_score_range
					(V_alig_file, V_gene, Vrange, false, indexed_seqlist_all, sorted_alignments);
			for (auto& it : sorted_alignments)
				v_alignments[it.first]=it.second.second[V_gene];
		}

		if (Jrange < 0) //if Jrange negative, use hard threshold to filter alignments
			j_alignments = read_alignments_seq_csv(J_alig_file, Jthres, false);
		else{
			unordered_map<int,pair<string,unordered_map<Gene_class,vector<Alignment_data>>>> sorted_alignments;
			sorted_alignments = read_alignments_seq_csv_score_range
					(J_alig_file, J_gene, Jrange, false, indexed_seqlist_all, sorted_alignments);
			for (auto& it : sorted_alignments)
				j_alignments[it.first]=it.second.second[J_gene];
		}

		for (auto& it : j_alignments)
					for (auto& gene : it.second)
						gene.offset = gene.offset - plen; //change J alignment to account for added palindromic ins

		if (prob){
			if (N>0){ // if num of seq were specified, then limit seq list
				N = min(N, int(indexed_seqlist_all.size()));
				indexed_seqlist_all.resize(N);
			}
			if (!with_err) std_mod.error_rate = 0;
			Model_allVJ mod (std_mod);
			cout << "Calculating probabilities" << endl;

			Model_allVJ counter;
			vector<double> P;
			if (features.empty())
				tie(counter,P) = marg_prob_allsq (indexed_seqlist_all, v_alignments, j_alignments, mod, dbg);
			else{
				cout << "opening text file to record following features: " << features << endl;
				ofstream featfile(work_folder + results_path + pool_name + "_features.csv");
				tie(counter,P) = marg_prob_allsq (indexed_seqlist_all, v_alignments, j_alignments, mod,
						features, featfile, dbg);
			}
			cout << "Saving probabilities to: " << work_folder + results_path + pool_name + "_prob.csv" << endl;
			write_seq_prob(P, indexed_seqlist_all, work_folder + results_path + pool_name);
		}

		if (infer){
			// creating new list for sequences that had at least one good V and J alignment
			cout << "keeping only sequences which had at least one good V and J alignment" << endl;
			vector<pair<const int, const string>> indexed_seqlist;
			for (auto& it : indexed_seqlist_all){
				if ((v_alignments.find(it.first)!=v_alignments.end())
						& (j_alignments.find(it.first)!=j_alignments.end())){
					indexed_seqlist.push_back(pair<const int , const string >(it.first , it.second));
				}
			}
			cout << "dropped " << indexed_seqlist_all.size() - indexed_seqlist.size() << " reads"<< endl;

			if (N>0){ // if num of seq were specified, then limit seq list
				N = min(N, int(indexed_seqlist.size()));
				indexed_seqlist.resize(N);
			}

			cout << "number of sequences for inference: " << indexed_seqlist.size()  << endl;

			// infer model
			Model_allVJ mod (std_mod);
			cout << "Starting inference" << endl;
			pool_name = pool_name + suf;

			Model_allVJ output_model = infer_HMM (indexed_seqlist, v_alignments, j_alignments,
					 mod, work_folder + results_path + pool_name, max_iter, history, dbg);

			Standard_Model output_model_std(output_model);
			ofstream outfile(work_folder + results_path + pool_name + "_model.txt");
			output_model_std.output(outfile);
		}
	}
}
