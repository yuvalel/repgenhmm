/*
 * Utils.cpp

 *
 *  Created on: Apr 9, 2015
 *      Author: quentin
 */

#include "Utils.h"

using namespace std;

/////Utilitaries
ostream& operator<<(ostream& os , Gene_class gc){
	switch(gc){
	case V_gene:os<<"V_gene";break;
	case VD_genes:os<<"VD_genes";break;
	case D_gene:os<<"D_gene";break;
	case DJ_genes:os<<"DJ_gene";break;
	case VJ_genes:os<<"VJ_gene";break;
	case J_gene:os<<"J_gene";break;
	case VDJ_genes:os<<"VDJ_genes";break;
	case Undefined_gene: os<<"Undefined_gene";break;
	}
	return os;
}

ostream& operator<<(ostream& os , Seq_side ss){
	switch(ss){
	case Five_prime:os<<"Five_prime";break;
	case Three_prime:os<<"Three_prime";break;
	case Undefined_side:os<<"Undefined_side";break;
	}
	return os;
}



