/*
 * ModelallVJ.cpp
 *
 *  Created on: Feb 17, 2015
 *      Author: yuvalel
 */

#include "ModelallVJ.h"
#include <algorithm>
using namespace std;

Model_allVJ::Model_allVJ() : error_rate(0), non_error_rate(0), maxVdel(0), maxJdel(0), maxIns(0){}

Model_allVJ::Model_allVJ(ChainT ct) : chain_type(ct), error_rate(0), non_error_rate(0), maxVdel(0), maxJdel(0), maxIns(0){}

Model_allVJ::Model_allVJ(Standard_Model SM){

	Vs = SM.Vs;
	Js = SM.Js;
	Ds = SM.Ds;

	chain_type = SM.chain_type;
	error_rate = SM.error_rate;
	non_error_rate = 1 - SM.error_rate;

	nt_bias = SM.nt_bias;
	nt_biasVD = SM.nt_biasVD;
	nt_biasDJ = SM.nt_biasDJ;

	pVDJ = SM.pVDJ;

	maxVdel = SM.maxVdel;
	maxJdel = SM.maxJdel;
	maxIns = SM.maxIns;
	minDlen = SM.minDlen;

	for (const auto& Vit : *Vs){
		lenV[Vit.first] = Vit.second.length();
		double normtmp = 1;
		auto delVit = SM.pdelV.at(Vit.first).end();
		for (unsigned v=0;v<Vit.second.length();v++){
			if ((v<(Vit.second.length() - SM.pdelV.at(Vit.first).size())) || (normtmp == 0)) //if before deletions
				transV[Vit.first].T(sV,v+1,sV,v) = 1;
			else{
				delVit--;
				transV[Vit.first].T(sG,1,sV,v) = min(*delVit / normtmp, 1.0);
				if (v<(Vit.second.length()-1)) //if not last V state
					transV[Vit.first].T(sV,v+1,sV,v) = 1 - transV[Vit.first].T(sG,1,sV,v);
				normtmp -= *delVit;
			}
		}
	}

	//lambda function to do all insertion assignments
	auto Ins2Trans = [&] (int Gbefore, int Gafter, StateType Istate, vector<double> p_ins){
		trans.T(sG,Gafter,sG,Gbefore) = p_ins.at(0);
		trans.T(Istate,0,sG,Gbefore) = 1 - p_ins.at(0);
		double normtmp = 1 - p_ins.at(0);
		for (unsigned i=1; normtmp>0 && (i<p_ins.size()-1); i++){
			trans.T(sG,Gafter,Istate,i-1) = p_ins.at(i) / normtmp;
			trans.T(Istate,i,Istate,i-1) = 1 - trans.T(sG,Gafter,Istate,i-1);
			normtmp -= p_ins.at(i);
		}
		trans.T(sG,Gafter,Istate,p_ins.size()-2) = 1;

	};

	if (chain_type==Light){
		Ins2Trans(1, 4, sI, SM.pins);
	}
	if (chain_type==Heavy){
		Ins2Trans(1, 2, sI, SM.pinsVD);
		Ins2Trans(3, 4, sN, SM.pinsDJ);


		for (const auto& Dit : *Ds){
			lenD[Dit.first] = Dit.second.length(); //write length of D gene
			for (int l=0;l<=lenD[Dit.first];l++)
				for (int r=0;r<=lenD[Dit.first]-l;r++)
					transD[Dit.first].T(sD,0,l,r,sG,2) = SM.pdelD.at(Dit.first)[l][r];
		}
	}

	for (const auto& Jit : *Js){
		lenJ[Jit.first] = Jit.second.length();
		auto delJit = SM.pdelJ.at(Jit.first).begin();
		for (unsigned j=0;j<Jit.second.length();j++){
			if (j<Jit.second.length()-1)
				transJ[Jit.first].T(sJ,j+1,sJ,j) = 1;
			if (j<(SM.pdelJ.at(Jit.first).size())){
				transJ[Jit.first].T(sJ,j,sG,4) = *delJit ;
				delJit++;
			}
			else
				transJ[Jit.first].T(sJ,j,sG,4) = 0 ;
		}
	}
}

void Model_allVJ::add_m(const Model_allVJ& m, double factor){ //adding a different model object, times a factor
	error_rate += factor * m.error_rate;
	non_error_rate += factor * m.non_error_rate;
	for(const auto& it : m.nt_bias) nt_bias[it.first] += factor * it.second;
	for(const auto& it : m.nt_biasVD) nt_biasVD[it.first] += factor * it.second;
	for(const auto& it : m.nt_biasDJ) nt_biasDJ[it.first] += factor * it.second;

	trans.add(m.trans, factor);
	for(const auto& it : m.transVDJ) transVDJ[it.first].add(it.second, factor);
	for(const auto& it : m.transV) transV[it.first].add(it.second, factor);
	for(const auto& it : m.transD) transD[it.first].add(it.second, factor);
	for(const auto& it : m.transJ) transJ[it.first].add(it.second, factor);
	for(const auto& it : m.pVDJ) pVDJ[it.first] += factor * it.second;
}

void Model_allVJ::add_m(const Model_1VJ& m, std::string v, std::string d, std::string j, double factor){ //adding a different VJ specific model object, times a factor
	error_rate += factor * m.error_rate;
	non_error_rate += factor * m.non_error_rate;
	for(const auto& it : m.nt_bias) nt_bias[it.first] += factor * it.second;
	for(const auto& it : m.nt_biasVD) nt_biasVD[it.first] += factor * it.second;
	for(const auto& it : m.nt_biasDJ) nt_biasDJ[it.first] += factor * it.second;
	//TODO: separate m.trans and then add components to prober trans in allVJ model. eliminate separate at normalizations
	transVDJ[make_tuple(v,d,j)].add(m.trans, factor);
	pVDJ[make_tuple(v,d,j)] += factor;

}

void Model_allVJ::mult(double factor){ //Multiplying all fields by a factor
	error_rate *= factor;
	non_error_rate *= factor;
	for(const auto& it : nt_bias) nt_bias[it.first] *= factor;
	for(const auto& it : nt_biasVD) nt_biasVD[it.first] *= factor;
	for(const auto& it : nt_biasDJ) nt_biasDJ[it.first] *= factor;

	trans.mult(factor);
	for(const auto& it : transVDJ) transVDJ[it.first].mult(factor);
	for(const auto& it : transV) transV[it.first].mult(factor);
	for(const auto& it : transD) transD[it.first].mult(factor);
	for(const auto& it : transJ) transJ[it.first].mult(factor);
	for(const auto& it : pVDJ) pVDJ[it.first] *= factor;
}

bool Model_allVJ::compare(const Model_allVJ& m, double thrs){
	bool same(true);
	same &= (error_rate - m.error_rate)<thrs;
	same &= (non_error_rate - m.non_error_rate)<thrs;
	for(const auto& it : m.nt_bias) same &= ((nt_bias.at(it.first) - m.nt_bias.at(it.first))<thrs);
	for(const auto& it : m.nt_biasVD) same &= ((nt_biasVD.at(it.first) - m.nt_biasVD.at(it.first))<thrs);
	for(const auto& it : m.nt_biasDJ) same &= ((nt_biasDJ.at(it.first) - m.nt_biasDJ.at(it.first))<thrs);
	same &= trans.compare(m.trans, thrs);
	for(auto& it : transVDJ) same &= it.second.compare(m.transVDJ.at(it.first), thrs);
	for(auto& it : transV) same &= it.second.compare(m.transV.at(it.first), thrs);
	for(auto& it : transD) same &= it.second.compare(m.transD.at(it.first), thrs);
	for(auto& it : transJ) same &= it.second.compare(m.transJ.at(it.first), thrs);
	return same;
}

Model_1VJ Model_allVJ::restrict(const std::string& v, const std::string& d, const std::string& j) const {
	Model_1VJ m;
	m.chain_type = chain_type;
	m.error_rate = error_rate;
	m.non_error_rate = non_error_rate;

	m.nt_bias = nt_bias;
	m.nt_biasVD = nt_biasVD;
	m.nt_biasDJ = nt_biasDJ;

	m.maxVdel = maxVdel;
	m.maxJdel = maxJdel;
	m.maxIns = maxIns;
	m.minDlen = minDlen;

	m.trans.merge(transV.at(v)); //merging specific V transitions into restricted model
	if (chain_type==Heavy) m.trans.merge(transD.at(d)); //merging specific J transitions into restricted model
	m.trans.merge(transJ.at(j)); //merging specific J transitions into restricted model
	m.trans.merge(trans); //merging non-specific VJ transitions into restricted model (if where not already in VJ specific)

	return m;
}

void Model_allVJ::model_from_counter(){

	error_rate = error_rate / (error_rate + non_error_rate);
	non_error_rate = 1 - error_rate;

	for(const auto& it : transVDJ) //going over VDJ specific transitions collected and separating it according to which genomic states are involved
		it.second.seperate(trans, transV[get<0>(it.first)], transD[get<1>(it.first)], transJ[get<2>(it.first)]);

	trans.normalize(chain_type);

	for(auto& it : transV) it.second.normalize(chain_type);
	for(auto& it : transD) it.second.normalize(chain_type);
	for(auto& it : transJ) it.second.normalize(chain_type);

	double sum = 0;
	for(const auto& it : pVDJ) sum += it.second;
	for(auto& it : pVDJ) it.second /= sum;

	sum = 0;
	for(const auto& it : nt_bias) sum += it.second;
	for(auto& it : nt_bias) it.second /= sum;

	sum = 0;
	for(const auto& it : nt_biasVD) sum += it.second;
	for(auto& it : nt_biasVD) it.second /= sum;

	sum = 0;
	for(const auto& it : nt_biasDJ) sum += it.second;
	for(auto& it : nt_biasDJ) it.second /= sum;

}

void Model_allVJ::write(std::ostream& strm){
	strm << (chain_type==Light?"Light":"Heavy") << endl;
	strm << error_rate << " " << non_error_rate;
	strm << endl;

	if (chain_type==Light){
		for (const auto& it : nt_bias)
			strm << it.first << " " << it.second << " ";
		strm << endl;
	}
	else{
		for (const auto& it : nt_biasVD)
			strm << it.first << " " << it.second << " ";
		strm << endl;
		for (const auto& it : nt_biasDJ)
			strm << it.first << " " << it.second << " ";
		strm << endl;
	}

	trans.write(strm);

	for (auto& it : transV){
		strm << it.first << endl;
		it.second.write(strm);
	}
	strm << "###" << endl;
	for (auto& it : transD){
		strm << it.first << endl;
		it.second.write(strm);
	}
	strm << "###" << endl;
	for (auto& it : transJ){
		strm << it.first << endl;
		it.second.write(strm);
	}
	strm << "###" << endl;
	for (auto& it : transVDJ){
		strm << get<0>(it.first) << " " << get<1>(it.first) << " " << get<2>(it.first) << " " << endl;
		it.second.write(strm);
	}
	strm << "###" << endl;
	for (auto& it : pVDJ)
		strm << get<0>(it.first) << " " << get<1>(it.first) << " " << get<2>(it.first) << " " << it.second << endl;
	strm << "###" << endl;
}

void Model_allVJ::read(std::istream& strm){
	string tmp;
	strm >> tmp;
	chain_type=(tmp=="Light"?Light:Heavy);

	strm >> tmp;
	error_rate = stod(tmp);
	strm >> tmp;
	non_error_rate = stod(tmp);
	if (chain_type==Light){
		for (int i=0;i<4;i++){
			char base;
			strm >> base;
			strm >> tmp;
			nt_bias[base] = stod(tmp);
		}

	}
	else{
		for (int i=0;i<4;i++){
			char base;
			strm >> base;
			strm >> tmp;
			nt_biasVD[base] = stod(tmp);
		}
		for (int i=0;i<4;i++){
			char base;
			strm >> base;
			strm >> tmp;
			nt_biasDJ[base] = stod(tmp);
		}
	}

	trans.read(strm);

	strm >> tmp;
	while (tmp!="###") {
		transV[tmp].read(strm);
		strm >> tmp;
	}
	strm >> tmp;
	while (tmp!="###") {
		transD[tmp].read(strm);
		strm >> tmp;
	}
	strm >> tmp;
	while (tmp!="###") {
		transJ[tmp].read(strm);
		strm >> tmp;
	}
	strm >> tmp;
	while (tmp!="###") {
		string V=tmp;
		strm >> tmp;
		string D=tmp;
		strm >> tmp;
		string J=tmp;
		transVDJ[make_tuple(V,D,J)].read(strm);
		strm >> tmp;
	}

	strm >> tmp;
	while (tmp!="###") {
		string V=tmp;
		strm >> tmp;
		string D=tmp;
		strm >> tmp;
		string J=tmp;
		strm >> tmp;
		pVDJ[make_tuple(V,D,J)]=stod(tmp);
		strm >> tmp;
		}

}
