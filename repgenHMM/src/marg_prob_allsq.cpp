/*
 * marg_prob_allsq.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: yuvalel
 */
#include <vector>
#include <tuple>
#include <unordered_map>
#include <cmath>
#include <sstream>

#include "Aligner.h"
#include "infer_funcs.h"

using namespace std;

pair<Model_allVJ,vector<double>> marg_prob_allsq (const vector<pair<const int,const string>>& indexed_seq_list,
		const unordered_map<int,vector<Alignment_data>>& V_indexed_alignments,
		const unordered_map<int,vector<Alignment_data>>& J_indexed_alignments,
		const Model_allVJ& mod, bool dbg){
	std::ostream strm(NULL);
	string features = "";
	return marg_prob_allsq (indexed_seq_list, V_indexed_alignments, J_indexed_alignments, mod, features, strm, dbg);
}


pair<Model_allVJ,vector<double>> marg_prob_allsq (const vector<pair<const int,const string>>& indexed_seq_list,
		const unordered_map<int,vector<Alignment_data>>& V_indexed_alignments,
		const unordered_map<int,vector<Alignment_data>>& J_indexed_alignments,
		const Model_allVJ& mod, const string& features, std::ostream& strm, bool dbg){
	bool output_features = !features.empty(); //whether to output sequence features to stream

	Model_allVJ marg_all=mod;
	marg_all.mult(0); //blank new model for the marginals

	int N = indexed_seq_list.size();
	vector<double> P_all(N);

	ostringstream strm_local;

    #pragma omp parallel private (strm_local) shared(P_all , marg_all, dbg, indexed_seq_list, V_indexed_alignments, J_indexed_alignments, mod) //num_threads(thrds)
	{
		vector<double> P_all_local(N);
		Model_allVJ marg_all_local=mod;
		marg_all_local.mult(0); //blank new model for the marginals




        #pragma omp single
		cout << "starting parallel loop, num of threads used:" << omp_get_num_threads() << endl;
        #pragma omp for schedule(dynamic) //nowait
		for (int num_seq = 0 ; num_seq<N; num_seq++){
			if ((!dbg) || (indexed_seq_list[num_seq].first<500)) //true) //
			{
				if ((num_seq % 1000) == 0 || dbg) cout << "sequence: #" << indexed_seq_list[num_seq].first << endl;
				Model_allVJ marg=mod;
				marg.mult(0); //blank model for marginals

				int current_seq = indexed_seq_list[num_seq].first;
				double P = 0;
				try {
					// check that current sequence indeed has some alignments for both V and J
					if ((V_indexed_alignments.find(current_seq)!=V_indexed_alignments.end())
							& (J_indexed_alignments.find(current_seq)!=J_indexed_alignments.end()))
						P = marg_prob_1sq(indexed_seq_list[num_seq].second,
								V_indexed_alignments.at(current_seq),
								J_indexed_alignments.at(current_seq), mod, marg, dbg);
				}
				catch (exception& e){
					cout << "Exception: " << e.what() << " in seq " << indexed_seq_list[num_seq].first << endl;
				}
				catch (...){
					cout << "Exception in seq " << indexed_seq_list[num_seq].first << endl;
				}
				{
					P_all_local[num_seq] = P;
					if (P>0)marg_all_local.add_m(marg);
					if (output_features){
						strm_local << "seq#: " << indexed_seq_list[num_seq].first << " ";
						Standard_Model SM(marg);
						SM.output(strm_local, features);
					}
				}
			}
		}
        #pragma omp critical(add_marginals)
		{
			for (int i=0;i<N;i++) P_all[i] += P_all_local[i];
			marg_all.add_m(marg_all_local);
			if (output_features){
				strm << strm_local.str();
			}
		}
	}




	marg_all.mult(1.0/N);
	return make_pair(marg_all, P_all);
}
