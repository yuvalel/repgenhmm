# repgenHMM readme #

# 1. Setup #

Repository contains a directory tree with a few folders. Source files (.cpp and .h) can be found in folder src/. To compile repgenHMM, a make file is included in folder Release/. Running "make" while in the Release folder will compile and create the repgenHMM executable.

Also included are two sample data files, for TCR alpha and beta chains, and genomic files acquired from IMGT database. Those should be placed in the file system so repgenHMM can find them. 

# 2. Examples #

In all following examples, genomic files were placed in folder ~/WORK/genomic/ and are called TRAV.fasta, TRAJ.fasta for alpha chains, and TRBV.fasta, TRVJ.fasta for beta chains. 

 
* Reading new alpha chain sequences from alpha_sample.txt file in folder ~/WORK/out_of_frame. Aligning them with no filtering of alignments. Notice that this can have long running time so aligning with no filtering should be done only for smaller samples. Saving index sequence file (sample1_indx.csv), alignment files (sample1_V_alig.csv and sample1_J_alig.csv) and best alignments file (sample1_best_alignments.txt) in folder ~/WORK/alignments/


```
#!bash
repgenHMM -alpha -a -stat -Vthres 0 -Jthres 0 -folder ~/WORK/ -geno genomic/TRA 
          -seq out_of_frame/alpha_sample.txt -pool alignments/sample1
```


* Inferring alpha sequences that were already aligned and saved as ~/WORK/alignments/sample1_indx.csv. Using only V alignments with scores above 100 and J alignments with scores above 200. Maximum possible number of insertions and deletions is 30 and 20, respectively. Saving model files and generation probabilities files as ~/WORK/results/sample1_model.txt and /WORK/results/sample1_prob.txt.


```
#!bash

repgenHMM -alpha -i -Vdel 30 -Jdel 20 -ins 30 -Vthres 100 -Jthres 200 
          -folder ~/WORK/ -geno genomic/TRA -pool alignments/sample1 -results results/
```

* Aligning and inferring beta sequences, using ranges for V and J and default insertion and deletion maxima. Sequences are read from file out_of_frame/beta_sample.txt. Index sequence file, alignment files and results files are all saved in folder beta/.

```
#!bash

repgenHMM -beta -a -i -Vrange 20 -Jrange 50 -folder ~/WORK/ -geno genomic/TRB 
          -seq out_of_frame/beta_sample.txt -pool beta/sample2
```

* Generating 10000 new beta sequences with errors using random generator seed set by current time, according to model in ~/WORK/results/sample2_model.txt. Cutting them to 150bp reads, saving them in ~/WORK/generated/ with file name beta + seed value used. Aligning them, also saving alignments in ~/WORK/generated. Calculating generation probabilities using same model, saving it in ~/WORK/generated/beta/ (since no results folder is given). 

```
#!bash

repgenHMM -beta -g -n 10000 -readlen 150 -a -prob -Vrange 20 -Jrange 50 
          -folder ~/WORK/ -geno genomic/TRB -model results/sample2_model.txt -pool generated/beta
```

* Generating 10000 new alpha sequences without errors using random generator seed 888, using model in ~/WORK/results/sample1_model.txt. Cutting them to 120bp reads, saving them in ~/WORK/generated/beta_synth888_indx.csv. Aligning them, also saving alignments in ~/WORK/generated. Inferring from them a new model, saved as results/beta_synth888_model.txt.

```
#!bash

repgenHMM -alpha -g -n 10000 -readlen 120 -a -i -seed 888 -Vthres 100 -Jthres 200 
          -folder ~/WORK/ -geno genomic/TRA -model results/sample1_model.txt -pool generated/beta -results results/
```

* Aligning alpha chain sequences in in_frame/alpha_sample3.fasta, and calculating generation probability for each sequence using model in results/beta_synth888_model.txt. Alignments and sequence probability files are saved in results/.

```
#!bash

repgenHMM -alpha -a -prob -Vthres 100 -Jthres 200 -folder ~/WORK/ -geno genomic/TRA 
          -seq in_frame/alpha_sample3.fasta -model results/beta_synth888_model.txt -pool alignment/sample3 -results results/
```
# 3. File types used: #

**Raw sequence files**

Can be either FASTA files (must end in .fasta) or plain text files containing just sequences (no headers). For inference those should be out of frame sequences and unique.

**Index sequence files**

.csv files (separated by semicolons ;) created by repgenHMM when aligning sequences. contain index numbers and sequences.

**Alignment files**

.csv files (separated by semicolons ;) created by repgenHMM when aligning sequences. Contain index numbers of sequences from the corresponding index file, temple gene name (V or J), alignment score, alignment offset and lists of deletions, insertions and mismatches in the alignment.

**Model files**

.txt files with all the parameters of the model. Human readable, but a Matlab script to read them into a Matlab structure is included.

**Sequence probability files**

Similar in structure to the index sequence files but with generation probabilities.


# 4. Command line parameters: #

## 4.1. Chain type (mandatory): ##

**-light** / **-alpha** : processing TCR alpha or BCR light chains.

**-heavy** / **-beta** : processing TCR beta or BCR heavy chains.


## 4.2. Modules: ##

**-g** : generating. 

Input is model file. Generate sequences according to model and saves them in file. Saves also uncut reads and the generating scenario for each sequence in separate files.

**-a** : aligning. 

Input is sequence file, either raw or already in index form. Align all sequences all to V and J genomic sequences and output alignment files including scores and offsets. Also outputs sequence index file.

**-stat** : alignment statistics. 

Input is alignments files. Generate files with best alignment for each sequences. Used to help set thresholds for inference.

**-prob** : probabilities. 

Input is model file and sequence file. Calculates probabilities of sequences according to model and outputs them to file.

**-i** : inference. 

Input is sequence file and alignments file. Runs inference, outputs model files and files with generation probability of each sequence, for the final iteration.

**-test** : testing.

Input is sequence, alignments and model files supplied along with reference results in archive, **test_regpegHMM_sample.zip**. Used for self-test of software. All input files should be placed in folder *test* in working folder. The only switches that need to, and can, be specified are chain type, genomic files and working folder.

The different modules (except **test**) can be combined, and will be executed in the order above. so for instance, using "-g -a -stat -i" will generate sequences according to some model, align them, output best alignments and then use the same alignments to infer generation model, saving it in a different file. It is often useful to separate the alignment step from the inference one in order to determine the suitable alignment thresholds.


## 4.3. Other ##
All other switches must be followed by a value which they set, except when it is stated otherwise.

### 4.3.1 File names: ###
Those affect where repgenHMM looks for files and saves them. All folders should be created by the user before execution.

**-folder** : Work folder path. all other files and paths use this folder as starting folder. Default is current directory.

**-geno** : Genomic sequences path and start of FASTA file name. The ending is "V.FASTA" for the V genes and similarly for D and J. 

**-seq** : Sequences file name. Not needed if sequences are generated or index file already exists.

**-pool** : Mandatory. Sets pool path and name in the form "folder/name". Used to set name of sequence index file (with suffix "_indx.csv") and alignments files (with suffix "_V_alig.csv" or "_J_alig.csv"). This is used both for writing and reading. Results are saved using "name" in the results folder (see below). For generated sequences, random seed is added to pool name.

**-results** : Folder for results. Model files and probabilities files are saved here using the name of the pool. If not given, uses the pool folder.

**-model** : Model file name. uniform model is generated if no file is not given.

**-suf** : Suffix to add to output model file (and sequence probability files).


### 4.3.2. Generation parameters: ###

those affect the generation module, with the -n also affecting alignment and inference.

**-n** : Number of reads to generate. If used with sequence file, down-sample the file to this number of sequences. For generation default value is 10000.

**-seed** : Random seed to control generation. If none is given, one is taken from system time. The seed number is added to the pool name and so appears in the file names of generated synthetic sequence files, alignments and inferred models.

**-readlen** : Read length to generate. Generated sequences are cut to this length from the J side. Default 100.

**-noerr** : Not follow by value. Used for generating sequences without errors, by ignoring the error rate. When ommited generated synthetic sequences will contain errors according to model's error rate.


### 4.3.3. Alignment parameters ###
Those control which alignments are saved during alignment step but also which are used for inference and probability calculation.

**-Vthres** : Threshold on V alignment score. Alignments below this value are dropped during alignment or before inference. Default is 0, no filtering. Important to set for alignment step to avoid long running time.

**-Jthres** : Threshold on J alignment score. Alignments below this value are dropped during alignment before inference. Default is 0, no filtering.

**-Vrange** : Different method to set the threshold for inference using the best alignment score. V alignments with score not more than Vrange below the maximum alignment score are used. Does not affect alignment.

**-Jrange** : J alignments with score not more than Jrange below the maximum alignment score are used for inference. Does not affect alignment.


### 4.3.4. Inference parameters ###
Those affect the inference procedure.

**-iter** : Number of iterations to run for inference. Default 10.

**-th** : Number of parallel threads to use. Default 4.

**-ins** : Maximum number of insertions. Default 40.

**-Vdel** : Maximum number of V deletions. Default 40.

**-Jdel** : Maximum number of J deletions. Default 30.

**-history**: Saves intermediate model files and probability list of sequences for each iteration.