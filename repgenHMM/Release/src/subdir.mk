################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Aligner.cpp \
../src/BW_1seq_VDJ.cpp \
../src/BW_1seq_VJ.cpp \
../src/IntStr.cpp \
../src/Model_1VJ.cpp \
../src/ModelallVJ.cpp \
../src/StandardModel.cpp \
../src/Transitions.cpp \
../src/Utils.cpp \
../src/infer_HMM.cpp \
../src/main.cpp \
../src/marg_prob_1sq.cpp \
../src/marg_prob_allsq.cpp \
../src/test.cpp 

OBJS += \
./src/Aligner.o \
./src/BW_1seq_VDJ.o \
./src/BW_1seq_VJ.o \
./src/IntStr.o \
./src/Model_1VJ.o \
./src/ModelallVJ.o \
./src/StandardModel.o \
./src/Transitions.o \
./src/Utils.o \
./src/infer_HMM.o \
./src/main.o \
./src/marg_prob_1sq.o \
./src/marg_prob_allsq.o \
./src/test.o 

CPP_DEPS += \
./src/Aligner.d \
./src/BW_1seq_VDJ.d \
./src/BW_1seq_VJ.d \
./src/IntStr.d \
./src/Model_1VJ.d \
./src/ModelallVJ.d \
./src/StandardModel.d \
./src/Transitions.d \
./src/Utils.d \
./src/infer_HMM.d \
./src/main.d \
./src/marg_prob_1sq.d \
./src/marg_prob_allsq.d \
./src/test.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -std=c++0x -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


