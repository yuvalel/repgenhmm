/*
 * hash_spec.h
 * hash functions for paris and tuples of objects
 *  Created on: Feb 28, 2015
 *      Author: yuvalel
 */

#ifndef SRC_HASH_SPEC_H_
#define SRC_HASH_SPEC_H_
#include<tuple>

namespace std {
template<typename T>
struct hash<std::tuple<T,T>>{
	std::size_t operator()(const std::tuple<T,T> &r) const
	{
		return (hash<T>()(std::get<0>(r))^hash<T>()(std::get<1>(r)));
	}
};
template<typename T>
struct hash<std::tuple<T,T,T>>{
	std::size_t operator()(const std::tuple<T,T,T> &r) const
	{
		return (hash<T>()(std::get<0>(r))^hash<T>()(std::get<1>(r))^hash<T>()(std::get<2>(r)));
	}
};
template<typename T>
struct hash<std::pair<T,T>>{
	std::size_t operator()(const std::pair<T,T> &r) const
	{
		return (hash<T>()(r.first)^hash<T>()(r.second));
	}
};

}


#endif /* SRC_HASH_SPEC_H_ */
