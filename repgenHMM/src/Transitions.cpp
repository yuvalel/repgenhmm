/*
 * Transitions.cpp
 *
 *  Created on: Feb 17, 2015
 *      Author: yuvalel
 */

#include "Transitions.h"

using namespace std;

Transitions::Transitions() {
	within=std::vector<double>(max_state);
	between=std::vector<double>(max_state);
}

double& Transitions::T(StateType t1, int n1, StateType t2, int n2) {
	return (t1==t2?within[t2+n2]:between[t1+n1+t2+n2-1]);
}

double Transitions::T(StateType t1, int n1, StateType t2, int n2) const {
	return (t1==t2?within[t2+n2]:between[t1+n1+t2+n2-1]);
}

double& Transitions::T(StateType t1, int n1, int l1, int r1, StateType t2, int n2) {
	return between[t1 + l1*max_dlen + r1 ];
}

double Transitions::T(StateType t1, int n1, int l1, int r1, StateType t2, int n2) const {
	return between[t1 + l1*max_dlen + r1 ];
}

void Transitions::divide_by(StateType t1, int n1, StateType t2, int n2, double factor){
	T(t1, n1, t2, n2) /= factor;
}

void Transitions::divide_by(StateType t1, int n1, int l1, int r1, StateType t2, int n2, double factor){
	T(t1, n1, l1, r1, t2, n2) /= factor;
}

void Transitions::add(const Transitions& t, double factor){
	for (int i=0; i<max_state; i++) within[i] += factor * t.within[i];
	for (int i=0; i<max_state; i++) between[i] += factor * t.between[i];
}

void Transitions::mult(double factor) {
	for (int i=0; i<max_state; i++) within[i] *= factor;
	for (int i=0; i<max_state; i++) between[i] *= factor;
}

void Transitions::merge(const Transitions& t){
	for (int i=0; i<max_state; i++) if (within[i]==0) within[i] = t.within[i];
	for (int i=0; i<max_state; i++) if (between[i]==0) between[i] = t.between[i];
}

void Transitions::copy_vals(const Transitions& t){
	for (int i=0; i<max_state; i++) within[i] = t.within[i];
	for (int i=0; i<max_state; i++) between[i] = t.between[i];
}

void Transitions::seperate(Transitions& T, Transitions& TV, Transitions& TD, Transitions& TJ) const{
	for (int i=0; i<max_state; i++){
		if (i>=sV && i<sJ) {TV.within[i] += within[i]; TV.between[i] += between[i];}
		else if (i>=sJ && i<sI) {TJ.within[i] += within[i]; TJ.between[i] += between[i];}
		else if (i>=sD) {TD.within[i] += within[i]; TD.between[i] += between[i];}
		else {T.within[i] += within[i]; T.between[i] += between[i];}
	}
}


void Transitions::normalize(ChainT chain_type){
	double sum=0;
	int v;
	// normalizing all transitions to G1 state
	for (v = 0; v<(sJ-sV) ;v++){
		sum = T(sV,v+1,sV,v) + T(sG,1,sV,v);
		if (sum>0){
			divide_by(sV,v+1,sV,v, sum);
			divide_by(sG,1,sV,v, sum);
		}
	}

	/*for (v = 0; (T(sV,v+1,sV,v)>0) && (v<(sJ-sV)) ;v++){
		sum = T(sV,v+1,sV,v) + T(sG,1,sV,v);
		divide_by(sV,v+1,sV,v, sum);
		divide_by(sG,1,sV,v, sum);
	}
	*/
	divide_by(sG,1,sV,v, T(sG,1,sV,v));

	// lambda function to do all insertion normalizations
	auto InsNorm = [&] (int Gbefore, int Gafter, StateType Istate){
		double sum=0;
		StateType Istate_after=(Istate==sI?sN:sD);
		sum = T(Istate,0,sG,Gbefore) + T(sG,Gafter,sG,Gbefore);
		if (sum>0){
			divide_by(Istate,0,sG,Gbefore,sum);
			divide_by(sG,Gafter,sG,Gbefore,sum);
		}
		int i;
		for (i = 0; (T(Istate,i+1,Istate,i)>0) && (i<(Istate_after-Istate));i++){
			sum = T(Istate,i+1,Istate,i) + T(sG,Gafter,Istate,i);
			divide_by(Istate,i+1,Istate,i,sum);
			divide_by(sG,Gafter,Istate,i,sum);
		}
		if (T(sG,Gafter,Istate,i)>0) divide_by(sG,Gafter,Istate,i, T(sG,Gafter,Istate,i));
	};

	if (chain_type==Light) InsNorm(1, 4, sI);
	if (chain_type==Heavy) {
		InsNorm(1, 2, sI);
		InsNorm(3, 4, sN);

		double tmp=0;
		for (int l=0;T(sD,0,l,0,sG,2)>0;l++)
			for (int r=0;T(sD,0,l,r,sG,2)>0;r++)
				tmp += T(sD,0,l,r,sG,2);

		for (int l=0;T(sD,0,l,0,sG,2)>0;l++)
			for (int r=0;T(sD,0,l,r,sG,2)>0;r++)
				divide_by(sD,0,l,r,sG,2,tmp);
	}

	int j;
	sum = 0;
	for (j = 0; (T(sJ,j,sG,4)>0) && (j<(sI-sJ)) ;j++)
		sum += T(sJ,j,sG,4);
	for (j = 0; (T(sJ,j,sG,4)>0) && (j<(sI-sJ)) ;j++)
		divide_by(sJ,j,sG,4, sum);
	for (j = 0; (T(sJ,j+1,sJ,j)>0) && (j<(sI-sJ)) ;j++)
		divide_by(sJ,j+1,sJ,j, T(sJ,j+1,sJ,j));
}

bool Transitions::compare(const Transitions& t, double thrs){
	bool same(true);
	for (int i=0; i<max_state; i++) same &= (within[i] - t.within[i]) < thrs;
	for (int i=0; i<max_state; i++) same &= (between[i] - t.between[i]) < thrs;
	return same;
}

void Transitions::write(std::ostream& strm){
	for (int i=0; i<max_state; i++) if (within[i]>0) strm << i << " " << within[i] << " ";
	strm << "##" << endl;
	for (int i=0; i<max_state; i++) if (between[i]>0) strm << i << " " << between[i] << " ";
	strm << "##" << endl;
}

void Transitions::read(std::istream& strm){
	string tmp;
	int i;
	strm >> tmp;
	while (tmp!="##") {
		i = stoi(tmp);
		strm >> tmp;
		within[i] = stod(tmp);
		strm >> tmp;
	}
	strm >> tmp;
	while (tmp!="##") {
		i = stoi(tmp);
		strm >> tmp;
		between[i] = stod(tmp);
		strm >> tmp;
	}


}
